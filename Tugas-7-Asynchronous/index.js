// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Tulis code untuk memanggil function readBooks di sini


var book1 = [{name: 'LOTR', timeSpent: 3000}]
var book2 = [{name: 'Fidas', timeSpent: 2000}]
var book3 = [{name: 'Kalkulus', timeSpent: 4000}]
var book4 = [{name: 'Komik', timeSpent: 1000}]


book1.forEach(book => readBooks (7000, book, callback => {
    console.log(callback)    
}))


book2.forEach(book => readBooks (9000, book, callback => {
    console.log(callback)    
}))


book3.forEach(book => readBooks (4000, book, callback => {
    console.log(callback)    
}))


book4.forEach(book => readBooks (10000, book, callback => {
    console.log(callback)    
}))

