//Soal 1
console.log("Soal 1: Arrow Function")

const luas_persegi_panjang = function (p, l) {
    return p * l
} 

console.log("Luas Persegi Panjang" + " " + "=" + " " + (luas_persegi_panjang(2, 4)))

let keliling_persegi_panjang = function (p, l) {
    return 2 * (p + l)
} 

console.log("Keliling Persegi Panjang" + " " + "=" + " " + (keliling_persegi_panjang(2, 4)))


//Soal 2
console.log("\n")
console.log("Soal 2: Arrow Function & Object Literals")

const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()


  //Ubah ke ES6
  console.log("\n")
  console.log("Arrow Function")

const literal = function(firstName,lastName) {
    return (firstName + " " + lastName)
}

console.log(literal("William","Imoh"))

  console.log("\n")
  console.log("object literals")

  const fullName = "Wiliam Imoh"
  const Wiliam = {fullName}
  console.log(Wiliam)


//Soal 3
console.log("\n")
console.log("Soal 3: Metode Destructuring ES6")

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };

  const {firstName, lastName, address, hobby} = newObject

  console.log(firstName, lastName, address, hobby)


//Soal 4
console.log("\n")
console.log("Soal 4: Array Spreading ES6")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combinedArray = [west, east]

console.log(combinedArray)


//Soal 5
console.log("\n")
console.log("Soal 5: Template Literals ES6")


const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const theString = `${before}`

console.log(theString)

